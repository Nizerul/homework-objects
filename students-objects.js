var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

/*Выводим список студентов
  Отказался от filter*/

var students = [];
studentsAndPoints.forEach(function(student, i){
    if( i == 0 || i % 2 == 0 ){
      students.push(
        {name: student, 
         point: studentsAndPoints[i+1],
         show: function(){
           console.log('Студент %s набрал %s баллов', this.name, this.point);
         }
        })
    }
})

students.forEach(function(student){
	student.show();
});

console.log('');

//Добавляем студентов в список

students.push(
        {name: "Николай Фролов", 
         point: 0,
         show: function(){
           console.log('Студент %s набрал %s баллов', this.name, this.point);
         }
        });
       
students.push(
        {name: "Олег Боровой", 
         point: 0,
         show: function(){
           console.log('Студент %s набрал %s баллов', this.name, this.point);
         }
        });

/*Увеличиваем баллы студентов
  Отказался от map*/

students.forEach(function(student){
	if(student.name === "Ирина Овчинникова" || student.name === "Александр Малов"){
  	student.point += 30;
  }
  if(student.name === "Николай Фролов"){
  	student.point += 10;
  }
})

//Выводим список студентов, у которых больше 30 баллов

students.forEach(function(student){
	if(student.point >= 30){
  	student.show();
  }
})

//Добавляем поле worksAmount

students.forEach(function(student){
	student.worksAmount = student.point / 10;
});

console.log('');

/*Дополнительное задание
  Сделал вывод иным способом*/

students.findByName = function(name){
  return this.find(function(student){
  	return student.name === name;
  });
}

var result = students.findByName('Ирина Овчинникова');

if (result !== undefined){
    result.show();
}else{
    console.log(result);
}